window.addEventListener("load", function() {
    var counties = document.getElementsByTagName("path");
    var button_container = document.getElementById("button_container")
    for (var i = 0; i < counties.length; i++) {
        var county = counties[i]
        var button = document.createElement("Button")
        button.setAttribute("id", county.getAttribute("id") + "_button")
        var text = document.createTextNode(county.getAttribute("name"))
        button.appendChild(text)
        button_container.appendChild(button)
        button.addEventListener("mouseover", function( event ) {
            var county_id = event.target.getAttribute("id").replace("_button", "")
            colourCounty(county_id)
        })
        button.addEventListener("mouseleave", function( event ) {
            var county_id = event.target.getAttribute("id").replace("_button", "")
            uncolourCounty(county_id)
        })
        county.addEventListener("mouseover", function( event ) {
            var event2 = new Event("mouseover")
            var button_id = event.target.getAttribute("id") + "_button"
            var button = document.getElementById(button_id)
            button.classList.add("button_hovered")
            colourCounty(event.target.getAttribute("id"))
        })
        county.addEventListener("mouseleave", function( event ) {
            var event2 = new Event("mouseleave")
            var button_id = event.target.getAttribute("id") + "_button"
            var button = document.getElementById(button_id)
            button.classList.remove("button_hovered")
            uncolourCounty(event.target.getAttribute("id"))
        })
    }
})

function colourCounty(id) {
    var element = document.getElementById(id)
    element.classList.add("hovered_county")
}

function uncolourCounty(id) {
    var element = document.getElementById(id)
    element.classList.remove("hovered_county")
}


